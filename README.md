# IPSBackupRestore

&#9888; ACHTUNG! BETA-Version! Benutzung auf eigene Gefahr! &#9888;

!!! Restore ist noch nicht implementiert !!!

----
Modul zur Kapselung von Backup und Restore-Funktionen für IP-Symcon 4+
----

Bisher getestet auf einem Raspberry Pi 3 Model B+ mit Raspbian.
USB-Stick: /dev/sda1 -> /mount/usbstick

----
TODO:

- RESTORE implementieren!
- Verwalten der Backups (insb. löschen alter Backups)
